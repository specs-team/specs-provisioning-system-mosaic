package deployer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;


public class Deployer {

///***CONFIGURE 
	private final static String node_ip = "192.168.178.187";
	private final static String jar_repo = "http://192.168.178.1:8080/MosaicDeployer/";
////*****END OF CONFIGURE

	
/******************************************************************************************/
	private final static String placeHolder = "&_repo_&";
	private final static String server = "http://"+node_ip+":31808/";
	private final static String json = "src/main/java/deployer/mosaic-provisioning-parametrico.json";
	private final static String jar = "provisioning_system-mosaic-0.6.0-jar-with-dependencies.jar";
	
	public static void main(String[] args) throws Exception{

		Gson gson = new Gson();
		String list = list();
		System.out.println("current components.. " + list );	
		Answer a = gson.fromJson(list, Answer.class);

		System.out.println("deleting...");
		for (String key : a.keys){
			delete(key);
		}		

		String content = new Scanner(new File(json)).useDelimiter("\\A").next();
		content = content.replaceAll(placeHolder, jar_repo+jar);
			 
		System.out.println("let's create new components... ");
		String created = create(content);
		System.out.println(created);
		System.out.println("done!");
	}


	public class Answer {
		String ok;
		String [] keys;
	}


	private static String create (String _json) throws ClientProtocolException, IOException {

		String url = server + "v1/processes/create" ;

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		
		post.setHeader("Content-Type", "application/json");
		post.setEntity(new StringEntity(_json));

		HttpResponse response = httpclient.execute(post);
		InputStream content;

		content = response.getEntity().getContent();

		BufferedReader reader = new BufferedReader(new InputStreamReader(content));

		return reader.readLine();

	}

	private static String list () throws ClientProtocolException, IOException {


		String url = server + "v1/processes" ;
		InputStream content;

		HttpClient httpclient = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);

		HttpResponse response = httpclient.execute(get);
		content = response.getEntity().getContent();

		BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		return reader.readLine();
	}


	private static String delete (String id) throws ClientProtocolException, IOException {


		String url = server + "v1/processes/stop?key="+id ;
		InputStream content;

		HttpClient httpclient = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);

		HttpResponse response = httpclient.execute(get);
		content = response.getEntity().getContent();

		BufferedReader reader = new BufferedReader(new InputStreamReader(content));

		return reader.readLine();

	}


}


