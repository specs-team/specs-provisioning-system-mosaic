package eu.specs_project.provisioning.mosaic.cloudlets.frontend;

import java.util.HashMap;
import java.util.UUID;

import com.google.gson.Gson;

import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.AmqpQueueConsumeCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueueConsumerConnector;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueueConsumerConnectorFactory;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueuePublisherConnector;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueuePublisherConnectorFactory;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackArguments;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.core.ICallback;
import eu.mosaic_cloud.cloudlets.core.ICloudletController;
import eu.mosaic_cloud.cloudlets.tools.DefaultAmqpPublisherConnectorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultAmqpQueueConsumerConnectorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultCloudletCallback;
import eu.mosaic_cloud.platform.core.configuration.ConfigurationIdentifier;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.platform.core.utils.JsonDataEncoder;
import eu.mosaic_cloud.platform.core.utils.PlainTextDataEncoder;
import eu.mosaic_cloud.tools.callbacks.core.CallbackCompletion;
import eu.specs_project.provisioning.mosaic.temp.ConfigStatus;

public class MiniHTTPgwFrontend{
		
	public static class MiniHTTPgwContext{
		ICloudletController<MiniHTTPgwContext> cloudlet;
		IConfiguration configuration;
		IAmqpQueueConsumerConnector<String, Void> mHTTPGWconsumer;
		IAmqpQueueConsumerConnector<ConfigStatus, Void> createAckConsumer;
		HashMap<UUID, HashMap<String, String>> clusterAttributes;
		IAmqpQueuePublisherConnector<HashMap, UUID> createClusterPublisher;
		IAmqpQueuePublisherConnector<HashMap, UUID> configClusterPublisher;
		IAmqpQueuePublisherConnector<HashMap, Void> destroyClusterPublisher;
		IAmqpQueuePublisherConnector<HashMap, UUID> startJobPublisher;
	}
	
	
	public static final class MiniHTTPgwAmqpConsumerCallback extends
			DefaultAmqpQueueConsumerConnectorCallback<MiniHTTPgwContext, String, Void> {
	
		public CallbackCompletion<Void> consume(final MiniHTTPgwContext context, final AmqpQueueConsumeCallbackArguments<String> arguments){
			final String message = arguments.getMessage();
			CommandWrapper action = new Gson().fromJson(message, CommandWrapper.class);
			String command = action.getCommand();
			this.logger.info("received from mHTTPgw message with command "+command+"...");
			HashMap<String, String> attributes = action.getAttributes();
			UUID id = UUID.randomUUID();
			attributes.put("id", id.toString());
			context.clusterAttributes.put(id, attributes);
			if(command.equalsIgnoreCase("create")){
				
				context.createClusterPublisher.publish(attributes, id);
			}
			else if(command.equalsIgnoreCase("destroy")){
				
			}
			else if(command.equalsIgnoreCase("create_job")){
				context.startJobPublisher.publish(attributes);
			}
			
			context.mHTTPGWconsumer.acknowledge(arguments.getToken());
			return ICallback.SUCCESS;
		}
	}	
	
	public static final class CreateAckAmqpConsumerCallback extends
		DefaultAmqpQueueConsumerConnectorCallback<MiniHTTPgwContext, ConfigStatus, Void> {
		
		
		
		@Override
		public CallbackCompletion<Void> consume(MiniHTTPgwContext context,
				AmqpQueueConsumeCallbackArguments<ConfigStatus> arguments) {

			this.logger.info("received ACK of created cluster, configuring cluster...");
			ConfigStatus status = arguments.getMessage();
			if(status.isContinueForConfig())
				;//context.configClusterPublisher.publish(context.clusterAttributes.get(status.getId()));
			else
				;
			context.createAckConsumer.acknowledge(arguments.getToken());
			return ICallback.SUCCESS;
		}
	}
		
	public static final class LifeCycleHandler extends DefaultCloudletCallback<MiniHTTPgwContext>{
		
		public CallbackCompletion<Void> destroy (final MiniHTTPgwContext context,
				final CloudletCallbackArguments<MiniHTTPgwContext> arguments){
			this.logger.info ("destroying cloudlet and components...");
			return CallbackCompletion.createAndChained(context.mHTTPGWconsumer.destroy(), context.createClusterPublisher.destroy(),
					context.createAckConsumer.destroy(), context.configClusterPublisher.destroy(), context.startJobPublisher.destroy());
		}
		
		
		public CallbackCompletion<Void> initialize (final MiniHTTPgwContext context, final CloudletCallbackArguments<MiniHTTPgwContext> arguments){
			this.logger.info("initializing mHTTPgw frontend...");
			context.cloudlet = arguments.getCloudlet ();
			final IConfiguration configuration = context.cloudlet.getConfiguration();
			final IConfiguration mHTTPgwConsumerConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("queuemhttpgw.consumer"));
			final IConfiguration createAckConsumerConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("create.ack.consumer"));
			final IConfiguration createClusterPublisherConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("create.publisher"));
			final IConfiguration configClusterPublisherConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("config.publisher"));
			final IConfiguration startJobPublisherConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("startjob.publisher"));
			context.clusterAttributes = new HashMap<UUID, HashMap<String, String>>();
			context.mHTTPGWconsumer = context.cloudlet.getConnectorFactory(IAmqpQueueConsumerConnectorFactory.class).create(mHTTPgwConsumerConfiguration,
					String.class, PlainTextDataEncoder.create(), new MiniHTTPgwAmqpConsumerCallback(), context);
			context.createAckConsumer = context.cloudlet.getConnectorFactory(IAmqpQueueConsumerConnectorFactory.class).create(createAckConsumerConfiguration,
					ConfigStatus.class, JsonDataEncoder.create(ConfigStatus.class), new CreateAckAmqpConsumerCallback(), context);
			context.createClusterPublisher = context.cloudlet.getConnectorFactory(IAmqpQueuePublisherConnectorFactory.class).create(createClusterPublisherConfiguration,
					HashMap.class, JsonDataEncoder.create(HashMap.class), new DefaultAmqpPublisherConnectorCallback<MiniHTTPgwContext, HashMap, UUID>(), context);
			context.configClusterPublisher = context.cloudlet.getConnectorFactory(IAmqpQueuePublisherConnectorFactory.class).create(configClusterPublisherConfiguration,
					HashMap.class, JsonDataEncoder.create(HashMap.class), new DefaultAmqpPublisherConnectorCallback<MiniHTTPgwContext, HashMap, UUID>(), context);
			context.startJobPublisher = context.cloudlet.getConnectorFactory(IAmqpQueuePublisherConnectorFactory.class).create(startJobPublisherConfiguration,
					HashMap.class, JsonDataEncoder.create(HashMap.class), new DefaultAmqpPublisherConnectorCallback<MiniHTTPgwContext, HashMap, UUID>(), context);
			return CallbackCompletion.createAndChained(context.mHTTPGWconsumer.initialize(), context.createClusterPublisher.initialize(),
					context.createAckConsumer.initialize(), context.configClusterPublisher.initialize(), context.startJobPublisher.initialize());
		}
		@Override
		public CallbackCompletion<Void> initializeSucceeded(
				MiniHTTPgwContext context,
				CloudletCallbackCompletionArguments<MiniHTTPgwContext> arguments) {
			// TODO Auto-generated method stub
			this.logger.info("mHTTPgw frontend initialized successfully");
			return ICallback.SUCCESS;
		}
	}
	
}
