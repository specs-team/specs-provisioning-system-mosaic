package eu.specs_project.provisioning.mosaic.cloudlets.frontend;

import java.util.HashMap;

public class CommandWrapper {

	private String command;
	private HashMap<String, String> attributes;
	
	public CommandWrapper(){
		command = null;
		attributes = new HashMap<String, String>();
	}
	
	public CommandWrapper(String command){
		this.command = command;
		attributes = new HashMap<String, String>();
	}

	public String getCommand() {
		return command;
	}

	public HashMap<String, String> getAttributes() {
		return attributes;
	}

/*	public void setCommand(String command) {
		this.command = command;
	}

	public void setAttributes(HashMap<String, String> attributes) {
		this.attributes = attributes;
	}
	
	*/
	
}
