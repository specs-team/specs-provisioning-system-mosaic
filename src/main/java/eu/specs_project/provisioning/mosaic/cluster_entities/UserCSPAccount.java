package eu.specs_project.provisioning.mosaic.cluster_entities;

import java.util.UUID;

public class UserCSPAccount {

	private final UUID id = UUID.randomUUID();
	
	public UserCSPAccount(){
	}
	
	public UserCSPAccount(UUID provider, String accountName){
		this.provider=provider;
		this.accountName=accountName;
	}
	
	public UserCSPAccount(UUID provider, String accountName, String providerUsername, String providerPassword){
		this.provider=provider;
		this.accountName=accountName;
		this.providerUsername=providerUsername;
		this.providerPassword=providerPassword;
	}	
	
	public UUID getProvider() {
		return provider;
	}

	public void setProvider(UUID provider) {
		this.provider = provider;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getProviderUsername() {
		return providerUsername;
	}

	public void setProviderUsername(String providerUsername) {
		this.providerUsername = providerUsername;
	}

	public String getProviderPassword() {
		return providerPassword;
	}

	public void setProviderPassword(String providerPassword) {
		this.providerPassword = providerPassword;
	}

	public UUID getId(){
		return id;
	}
	
	public String getDescription() {
		return description;
	}


	private UUID provider;
	private String accountName; // non e' primary key, ma unique per utente
	private String providerUsername;
	private String providerPassword;
	private String description;
	
}
