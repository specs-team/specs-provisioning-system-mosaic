package eu.specs_project.provisioning.mosaic.temp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScriptExecutionResult {

	private ScriptExecutionOutcome outcome;
	private Map<String, String> additionalInfo;
	private Map<String, List<String>> outputMap;
	private List<ClusterNode> nodesWithError;
	
	public ScriptExecutionResult(){
		additionalInfo = new HashMap<String, String>();
		nodesWithError = new ArrayList<ClusterNode>();
		outputMap = new HashMap<String, List<String>>();
		outcome = ScriptExecutionOutcome.OK;
	}
	
	public ScriptExecutionResult(ScriptExecutionOutcome outcome,
			Map<String, String> additionalInfo, List<ClusterNode> nodesWithError, Map<String, List<String>> outputMap) {
		super();
		this.outcome = outcome;
		this.additionalInfo = additionalInfo;
		this.outputMap = outputMap;
		this.nodesWithError = nodesWithError;
	}


	public ScriptExecutionOutcome getOutcome() {
		return outcome;
	}

	public void setOutcome(ScriptExecutionOutcome outcome) {
		this.outcome = outcome;
	}

	public Map<String, String> getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(Map<String, String> additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	public void addAdditionalInfo(String key, String value){
		additionalInfo.put(key, value);
	}

	public List<ClusterNode> getNodesWithError() {
		return nodesWithError;
	}

	public void setNodesWithError(List<ClusterNode> nodesWithError) {
		this.nodesWithError = nodesWithError;
	}
	
	public void addNodeWithError(ClusterNode nodeId){
		nodesWithError.add(nodeId);
	}
	
	public Map<String, List<String>> getOutputMap() {
		return outputMap;
	}

	public void setOutputMap(Map<String, List<String>> outputMap) {
		this.outputMap = outputMap;
	}

	public void addOutput(String key, List<String> output){
		outputMap.put(key, output);
	}

	public enum ScriptExecutionOutcome{
		OK, MASTER_ERROR, SLAVE_ERROR;
	}
}
