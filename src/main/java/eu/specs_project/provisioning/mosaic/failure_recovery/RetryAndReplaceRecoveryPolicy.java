package eu.specs_project.provisioning.mosaic.failure_recovery;

import org.jclouds.compute.RunNodesException;

import eu.specs_project.provisioning.mosaic.failure_recovery.RecoveryResult.RecoveryOutcome;
import eu.specs_project.provisioning.mosaic.temp.CloudService;
import eu.specs_project.provisioning.mosaic.temp.ClusterNode;
import eu.specs_project.provisioning.mosaic.temp.ScriptExecutionResult;
import eu.specs_project.provisioning.mosaic.temp.ScriptExecutionResult.ScriptExecutionOutcome;

public class RetryAndReplaceRecoveryPolicy extends RecoveryPolicy {

	private CloudService cs;
	private int numberOfRetries, maxRetriesOfPolicy;
	
	public RetryAndReplaceRecoveryPolicy(){
		super();
	}
	
	public RetryAndReplaceRecoveryPolicy(int numberOfRetries, int maxRetriesOfPolicy) {
		super();
		this.numberOfRetries = numberOfRetries;
		this.maxRetriesOfPolicy = maxRetriesOfPolicy;
	}
	
	@Override
	public RecoveryResult recovery(CloudService cs, ClusterNode node, ScriptExecutionResult result) {
		this.cs=cs;
		RecoveryResult recoveryResult = new RecoveryResult();
		recoveryResult.setOutput(result.getOutputMap().get(node.getId()));
		String userName = parameters.get("username");
		String password = parameters.get("password");
		String privateKeyDefaultUser = result.getAdditionalInfo().get("privateKeyDefaultUser");
		String publicKeyNewUser = result.getAdditionalInfo().get("publicKeyNewUser");
		String clusterName = parameters.get("clusterName");
		ClusterNode newNode;
		int i = 1, j = 0;
		do{
			result = cs.addNewUserViaExecForSingleNode(node, userName, password, publicKeyNewUser, privateKeyDefaultUser);			
			if(result.getOutcome().equals(ScriptExecutionOutcome.OK))
				break;
			i++;
		}
		while(i<=numberOfRetries);
		if(i>numberOfRetries){
			cs.destroySingleNode(node);
			recoveryResult.setOutcome(RecoveryOutcome.DESTROYED);
			do{
				try {
					newNode = cs.addNode(clusterName);
					newNode = replaceNodeWithUserInst(userName, password, publicKeyNewUser, privateKeyDefaultUser, clusterName);

				} catch (RunNodesException e) {
					newNode = null;
				}
				if(newNode!=null){
					recoveryResult.setNewNode(newNode);
					recoveryResult.setOutcome(RecoveryOutcome.REPLACED);
					break;
				}
				j++;
			}
			while(j<maxRetriesOfPolicy);
		}else{
			recoveryResult.setOutput(result.getOutputMap().get(node.getId()));
			recoveryResult.setOutcome(RecoveryOutcome.SUCCEEDED);
		}						
		return recoveryResult;
		
	}
	
	
	
	private ClusterNode replaceNodeWithUserInst(String userName,
			String password, String publicKeyNewUser,
			String privateKeyDefaultUser, String clusterName) throws RunNodesException {
		
		int i = 0;
		ClusterNode n =cs.addNode(clusterName);
		ScriptExecutionResult result;
		do{
			result = cs.addNewUserViaExecForSingleNode(n, userName, password, publicKeyNewUser, privateKeyDefaultUser);
			if(result.getOutcome().equals(ScriptExecutionOutcome.OK))
				return n;
			else
				i++;
		}while(i<numberOfRetries);
		cs.destroySingleNode(n);	
		
		return null;
	}
}
